<?php
require_once 'controller.php';
//Aqui é o teste da Branch teste
class Inicio extends controller {

    function __construct() {
        return '<p align="center">Teste de chamada do método inicio.</p>';
    }

    function mapa($locais, $qtd, $maiorValor, $menorValor, $titulo, $cor1, $cor2) {

        $Locais = $locais;
        $Qtd = $qtd;
        $maior_valor = $maiorValor;
        $menor_valor = $menorValor;
        $cor_maxima = $cor2;
        $cor_minima = $cor1;
        $titulo = $titulo;
        ?>
        <!-- Styles -->
        <style>
            #chartdiv {
                width: 100%;
                height: 500px;
            }
        </style>
        <!-- FIM Styles -->

        <script>

            //variáveis
            var i, arraylocais, arraytotal, maior_valor, menor_valor, cor_maxima, cor_minima;
            //recebe a string com elementos separados, vindos do PHP
            arraylocais = "<?php echo $Locais; ?>";
            arraytotal = "<?php echo $Qtd; ?>";
            maior_valor = "<?php echo $maior_valor; ?>";
            menor_valor = "<?php echo $menor_valor; ?>";
            cor_maxima = "<?php echo $cor_maxima; ?>";
            cor_minima = "<?php echo $cor_minima; ?>";
            titulo = "<?php echo $titulo; ?>";

            //transforma esta string em um array próprio do Javascript
            arraylocais = arraylocais.split("/");
            arraytotal = arraytotal.split("/");

            am4core.ready(function() {

                // Themes begin
                am4core.useTheme(am4themes_animated);
                // Themes end

                window.onload = function() {

                    // Definando Localizacao map
                    var currentMap = "brazilLow";
                    var title = titulo;


                    // Create map instance
                    var chart = am4core.create("chartdiv", am4maps.MapChart);

                    chart.titles.create().text = title;


                    // Definicoes do map (caminho)
                    chart.geodataSource.url = "<?php echo PUBLICO; ?>amcharts/geodata/json/" + currentMap + ".json";
                    chart.geodataSource.events.on("parseended", function(ev) {
                        var data = [];
                        for (i in arraylocais) {
                            data.push({
                                id: "BR-" + arraylocais[i],
                                value: arraytotal[i]
                            })
                        }
                        polygonSeries.data = data;
                    })

                    // Set projection
                    chart.projection = new am4maps.projections.Mercator();

                    // Create map polygon series
                    var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

                    //Definindo cor minima e max
                    polygonSeries.heatRules.push({
                        property: "fill",
                        target: polygonSeries.mapPolygons.template,
                        min: am4core.color(cor_minima),
                        max: am4core.color(cor_maxima),

                    });

                    // Make map load polygon data (state shapes and names) from GeoJSON
                    polygonSeries.useGeodata = true;

                    // Set up heat legend
                    let heatLegend = chart.createChild(am4maps.HeatLegend);
                    heatLegend.series = polygonSeries;
                    heatLegend.align = "right";
                    heatLegend.width = am4core.percent(25);
                    heatLegend.marginRight = am4core.percent(4);
                    heatLegend.minValue = 0;
                    heatLegend.maxValue = 40000000;
                    heatLegend.valign = "bottom";

                    // Definido titulo e valores legenda rodape
                    var minRange = heatLegend.valueAxis.axisRanges.create();
                    minRange.value = heatLegend.minValue;
                    minRange.label.text = 'Min: ' + menor_valor;
                    var maxRange = heatLegend.valueAxis.axisRanges.create();
                    maxRange.value = heatLegend.maxValue;
                    maxRange.label.text = 'Max: ' + maior_valor;

                    // Blank out internal heat legend value axis labels
                    heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
                        return "";
                    });

                    // Configure series tooltip
                    var polygonTemplate = polygonSeries.mapPolygons.template;
                    polygonTemplate.tooltipText = "{name}: {value}";
                    polygonTemplate.nonScalingStroke = true;
                    polygonTemplate.strokeWidth = 0.5;

                    // Configurando cor ao passar mouse em cima
                    var hs = polygonTemplate.states.create("hover");
                    hs.properties.fill = am4core.color(cor_maxima);



                };

            }); // end am4core.ready()
        </script>

        <?php
    }

    function carga() {
        $cor1 = '#' . $_POST['arrDadosForm']['cor1'];
        $cor2 = '#' . $_POST['arrDadosForm']['cor2'];

        require_once'../public/xlsx/simplexlsx.class.php';


        //Se tiver $_POST['arrDadosForm']['method'] quer dizer que vem de um formulario comum, se não, via  ajax

        if (isset($_POST['arrDadosForm']['method'])) {
            $path = utf8_encode($_FILES['file']['name'][0]);
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $tmp_name = $_FILES['file']['tmp_name'][0];
        } else {
            $path = utf8_encode($_FILES['file']['name']);
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            $tmp_name = $_FILES['file']['tmp_name'];
        }

        if ($ext != 'xlsx') {
            $this->redirect('2', 'inicio/home');
        } else {
            $nome = 'carga_de_dados.' . $ext;

            $destino = '../public/anexos/' . $nome;

            if (@move_uploaded_file($tmp_name, $destino)) {
                if (is_dir($destino)) {
                    $diretorio = dir($destino);
                    while (($arquivo = $diretorio->read()) !== false) {
                        if (strlen($arquivo) > 5) {
                            @$ler = $arquivo;
                        }
                    }
                    $diretorio->close();
                }
                $arquivo = $destino . $ler;

                $xlsx = SimpleXLSX::parse($arquivo);
                list( $cols, ) = $xlsx->dimension();
                $contador = 0;
                $arrEstados = '';
                $arrValor = '';
                $menorValor = '';
                $maiorValor = '';
                //Começo Loop pegar dados
                foreach ($xlsx->rows() as $k => $r) {

                    //Função POST
                    if (isset($_POST['arrDadosForm']['method'])) {

                        $Vestado = $_POST['arrDadosForm']['coluna_uf'];
                        $dado_estado = $_POST['arrDadosForm']['coluna_dado_uf'];
                        //Pulando a primeira linha(Titulo)
                        if (!empty($r[$Vestado])) {
                            if ($contador == 0) {
                                $contador++;
                            } else {
                                //começando a varrer a planilha
                                $estado = trim($r[$Vestado]);
                                $verifica_estado = strlen($estado);

                                //Verificando se o estado esta em SIGLA ou NOME
                                if ($verifica_estado > 2) {
                                    //Limpando estado(tirando acento e deixando minúsculo)
                                    $limpaEstado = strtolower($this->removeAcentuacao($estado));
                                    $estado = $this->Uf($limpaEstado);
                                }

                                //Començando com os primeiros Dados
                                if (empty($arrEstados)) {
                                    $arrEstados = $estado . '/';
                                    $arrValor = $r[$dado_estado] . '/';
                                    $menorValor = $r[$dado_estado];
                                    $maiorValor = $r[$dado_estado];
                                } else {
                                    $arrEstados .= $estado . '/';
                                    $arrValor .= $r[$dado_estado] . '/';
                                    //Comparando com a maior valor até o momento
                                    if ($r[$dado_estado] > $maiorValor) {
                                        $maiorValor = $r[$dado_estado];
                                    }
                                    //Comparando com o menor valor até o momento
                                    if ($r[$dado_estado] < $menorValor) {
                                        $menorValor = $r[$dado_estado];
                                    }
                                }
                            }
                        }
                    }
                    //Função Ajax
                    else {
                        //colocando encode para tirar ero de formatação
                        for ($i = 0; $i < count($r); $i++) {
                            $arrCorrigido[] = trim($r[$i]);
                            $casaCorrespondente[] = $i;
                        }
                        $linha['linha'] = $arrCorrigido;
                        $linha['var_casa'] = $casaCorrespondente;

                        echo json_encode($linha, JSON_UNESCAPED_UNICODE);
                        exit;
                    }
                }

                session_start();
                $_SESSION['arrEstado'] = $arrEstados;
                $_SESSION['arrValor'] = $arrValor;
                $_SESSION['maiorValor'] = $maiorValor;
                $_SESSION['menorValor'] = $menorValor;
                $_SESSION['cor1'] = $cor1;
                $_SESSION['cor2'] = $cor2;
                $_SESSION['titulo'] = $_POST['arrDadosForm']['titulo'];

                $this->redirect('', 'inicio/mapa');
            }
        }
    }

}

$oInicio = new Inicio();
$classe = 'Inicio';
$oBjeto = $oInicio;
@include_once '../application/request.php';
?>