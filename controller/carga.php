<?php
ini_set('max_execution_time', 864000);
ini_set('mssql.timeout', 864000);
ini_set('memory_limit', '3024M');

error_reporting(E_ALL ^ E_NOTICE);

session_start();
require_once '../application/configs/config.php';
require_once '../model/cBanco.php';
require_once 'log.php';
require_once'../public/xlsx/simplexlsx.class.php';

function diminuiDias1($data, $dias) {
    $data = explode("-", $data);
    $nova_data = mktime(0, 0, 0, $data[1], $data[2] - $dias, $data[0]);
    return strftime("%Y-%m-%d", $nova_data);
}

//Pegando data -30 dias            
$datahoje = date("Y-m-d ");
 echo $trintadias = diminuiDias1($datahoje, 30);

//$trintadias="2018-08-17";


//convertendo para formato BR para comparar na planilha
$dataverifi = str_replace("/", "-", $trintadias);
$dataverififim = date('d/m/Y', strtotime($dataverifi));



$objBanco = new cBanco();
mssql_connect('172.28.97.213:1437', 'usr_sei3', '123456789') or die('NÃÂ£o foi possÃÂ­vel conectar ao banco. Erro = ' . mysql_error());
mssql_select_db("SEI3") or die("NÃÂ£o foi possÃÂ­vel selecionar o banco de dados!");

/* Para arquivos maiores que 2M, mudar no phph.ini :  .
  max_execution_time = 36000
  max_input_time= 36000
  max_input_vars = 60000
  memory_limit= 1024M
  post_max_size = 1024M
  upload_max_filesize = 512M
  max_file_uploads = 50
 * 
 * 
 * 
 * mssql.timeout = 16000
 */

$consulta1=$objBanco->valida_execucao($trintadias);
$veri1=mysql_num_rows($consulta1);

$consulta2=$objBanco->valida_execucao2($trintadias);
$veri2=mysql_num_rows($consulta2);

if($veri1>0 || $veri2>0){
    echo 'Carga ja feita no dia de hoje';
    die;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml">
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        </head>
        <body>
            <table border="1">	
                <?php

                function soNumero($str) {
                    return preg_replace("/[^0-9]/", "", $str);
                }

                $pasta = '../public/recebe/';

                if (is_dir($pasta)) {
                    $diretorio = dir($pasta);

                    while (($arquivo = $diretorio->read()) !== false) {
                        if (strlen($arquivo) > 5) {
                            @$ler = $arquivo;
                        }
                    }

                    $diretorio->close();
                }

                $dir = $pasta . $ler;

                //FunÃÂ§ao para pegar Quantidade de processo no mes (O banco tem que ter valores do mes setados (1-12) com valores 0)
                $mesbanco = Array();
                $anobanco = Array();
                $totalbanco = Array();

                $nmesbanco = Array();
                $nanobanco = Array();
                $ntotalbanco = Array();

                //Pegando dados no banco sobre quantidade de processos por me
                $objBanco->sql = "SELECT * FROM total_mes";
                $rsProposto = $objBanco->query();

                while ($resultx = mysql_fetch_array($rsProposto)) {
                    $resultx['num_mes'];
                    $resultx['valort_mes'];
                    $resultx['ano'];

                    $mesbanco[] = (int) $resultx['num_mes'];
                    $anobanco[] = (int) $resultx['ano'];
                    $totalbanco[] = (int) $resultx['valort_mes'];
                }
                // FIM Pegando dados no banco sobre quantidade de processos por mes
                logMsg('Inicio da carga dia : ' . $trintadias, 'inicio');

                $xlsx = SimpleXLSX::parse($dir);
                list( $cols, ) = $xlsx->dimension();

                //Começo Loop pegar dados
                foreach ($xlsx->rows() as $k => $r) {

                    $horario = date('H:i:s');


                    if ($r[7] == $dataverififim) {
                        $nducomentoori = $r[4];

                        $valorori = $r[8];
                        $nfavorecido = $r[2];

                        $tam = strlen($nfavorecido);
                        if ($tam < 5) {
                            $nfavorecido = 'Numero nao Registrado';
                        } else {
                            $nfavorecido;
                        }

                        $nomefavorecido = $r[3];
                        $nomefavorecido = str_replace("'", "", "$nomefavorecido");
                        if ($nomefavorecido == 'Nao se aplica') {
                            $nomefavorecido = 'Nome nao Registrado';
                        } else {
                            $nomefavorecido;
                        }

                        $valor = $valorori;
                        $chekvalor = explode('.', $valor);
                        $checkvalor2 = explode(',', $valor);
                        if (count($chekvalor) >= 2 || count($checkvalor2) >= 2) {
                            $valor = soNumero($valor);

                            if (@strlen($checkvalor2[1]) == 1) {
                                $valor = number_format($valor, 1, ',', ' ');
                                $valor = soNumero($valor);
                            }
                        } else {
                            // echo 'Numero nÃÂ£o possui virgula e vou acrescentar 00:'.$valor;
                            @ $valor = number_format($valor, 2, ',', ' ');
                            $valor = soNumero($valor);
                        }

                        $valor2 = $r[9];
                        $chekvalor3 = explode('.', $valor2);
                        $checkvalor4 = explode(',', $valor2);
                        if (count($chekvalor3) >= 2 || count($checkvalor4) >= 2) {
                            $valor2 = soNumero($valor2);

                            if (@strlen($checkvalor4[1]) == 1) {
                                $valor2 = number_format($valor2, 1, ',', ' ');
                                $valor2 = soNumero($valor2);
                            }
                        } else {
                            // echo 'Numero nÃÂ£o possui virgula e vou acrescentar 00:'.$valor;
                            @$valor2 = number_format($valor2, 2, ',', ' ');
                            $valor2 = soNumero($valor2);
                        }

                        $valor = $valor + $valor2;
                        $docobsv = $r[6];
                        $dataproc = $r[7];
                        $idlocal = $r[0];

                        $explode = explode("/", $dataproc);
                        $mes = $explode[1];
                        $ano = $explode[2];
                        $emi_dia = $explode[2] . '-' . $explode[1] . '-' . $explode[0];

                        $ndocumentoex = explode('OB', $nducomentoori);
                        $ndocumento = $ano . 'OB' . $ndocumentoex[1];

                        for ($x = 0; $x < count($mesbanco); $x++) {
                            if ($mes == $mesbanco[$x]) {
                                if ($ano == $anobanco[$x]) {
                                    $totalbanco[$x] ++;
                                    break;
                                }
                            }
                        }

                        // Fim FunÃÂ§ao para pegar Quantidade de processo no mes 
// FIM Pegando informaÃÂ§ÃÂµes da planilha Excel
//INICIO Confrontar com SEI e PEgar informaÃÂ§ÃÂµes
//BUSCA PROCESSOS

                        if ($mes == '01') {
                            $mesconsulta = 'JANEIRO';
                        } else if ($mes == '02') {
                            $mesconsulta = 'FEVEREIRO';
                        } else if ($mes == '03') {
                            $mesconsulta = 'MARÇO';
                            $mesconsulta = utf8_decode($mesconsulta);
                        } else if ($mes == '04') {
                            $mesconsulta = 'ABRIL';
                        } else if ($mes == '05') {
                            $mesconsulta = 'MAIO';
                        } else if ($mes == '06') {
                            $mesconsulta = 'JUNHO';
                        } else if ($mes == '07') {
                            $mesconsulta = 'JULHO';
                        } else if ($mes == '08') {
                            $mesconsulta = 'AGOSTO';
                        } else if ($mes == '09') {
                            $mesconsulta = 'SETEMBRO';
                        } else if ($mes == '10') {
                            $mesconsulta = 'OUTUBRO';
                        } else if ($mes == '11') {
                            $mesconsulta = 'NOVEMBRO';
                        } else if ($mes == '12') {
                            $mesconsulta = 'DEZEMBRO';
                        }

                        //Buscar id dos bloco do mes referente
                        $Sql2 = "
SELECT * FROM ( SELECT bloco.id_bloco AS idbloco,bloco.id_unidade AS idunidade,bloco.descricao,bloco.sta_tipo AS statipo,uc.sigla AS siglaunidade,uc.descricao AS descricaounidade,bloco.sta_estado AS staestado,InfraRowCount = COUNT(*) OVER(),ROW_NUMBER() OVER ( ORDER BY bloco.id_bloco DESC) as InfraRowNumber  FROM bloco  INNER JOIN unidade uc  ON bloco.id_unidade=uc.id_unidade 
WHERE bloco.sta_tipo='A' 
AND bloco.sta_estado<>'C' 
AND (bloco.id_unidade=110000897 )) AS InfraTabela 
WHERE InfraRowNumber BETWEEN 1 AND 50  AND
descricao like '%$mesconsulta $ano%' ORDER BY InfraRowNumber";
                        $Result2 = mssql_query($Sql2);
                        
                      


                        $idbloco = Array();
                        while ($arDados2 = mssql_fetch_array($Result2)) {
                            $idbloco[] = $arDados2['idbloco'];
                        }



//Buscar processo analisado dentro do bloco
                         $Sql3 = "
  SELECT DISTINCT protocolo.protocolo_formatado AS protocoloformatado, usu_ger.sigla AS siglausuariogerador, protocolo.dta_geracao AS geracao, 
serie.nome AS nomeseriedocumento, tpd.nome AS nometipoprocedimentodocumento, ppd.protocolo_formatado AS protocoloformatadoprocedime003, 
ppd.id_protocolo as id_protocolo_sei,rel_bloco_protocolo.anotacao,dc.conteudo
FROM protocolo
 left JOIN ( procedimento p INNER JOIN tipo_procedimento tpp ON p.id_tipo_procedimento = tpp.id_tipo_procedimento ) ON protocolo.id_protocolo = p.id_procedimento 
 LEFT JOIN ( documento INNER JOIN serie ON documento.id_serie = serie.id_serie 
 INNER JOIN documento_conteudo as dc ON dc.id_documento = documento.id_documento
 INNER JOIN ( procedimento pd INNER JOIN tipo_procedimento tpd ON pd.id_tipo_procedimento = tpd.id_tipo_procedimento 
 INNER JOIN protocolo ppd ON pd.id_procedimento = ppd.id_protocolo ) ON documento.id_procedimento = pd.id_procedimento ) ON protocolo.id_protocolo = documento.id_documento 
 INNER JOIN unidade uni_ger ON protocolo.id_unidade_geradora = uni_ger.id_unidade 
 INNER JOIN usuario usu_ger ON protocolo.id_usuario_gerador = usu_ger.id_usuario 
  INNER JOIN rel_bloco_protocolo ON rel_bloco_protocolo.id_protocolo = protocolo.id_protocolo
   WHERE rel_bloco_protocolo.id_bloco IN ($idbloco[0],$idbloco[1]) AND rel_bloco_protocolo.anotacao like '%$ndocumento%' ORDER BY protocolo.dta_geracao DESC;                                  
    ";
                         
                               

                        $Result3 = mssql_query($Sql3);
                        $Result4 = mssql_query($Sql3);
                        $arDados3 = mssql_fetch_array($Result3);
                        $numarDados3 = mssql_num_rows($Result3);

                        if ($arDados3 <> false) {
                            echo 'encontrei : ' . $ndocumento;

                            //BUSCAR InformaÃÂ§ÃÂµes do  PROCESSOS
                            $idprotocolo = $arDados3['id_protocolo_sei'];
                            $numsei = $arDados3['protocoloformatadoprocedime003'];
                            $tipoproce = utf8_encode($arDados3['nometipoprocedimentodocumento']);
                            $uniorigem = @$arDados3['unidade_origem'];

                            if ($numarDados3 == 1) {
                                $numdocsei = $arDados3['protocoloformatado'];
                                $tipodocu = utf8_encode($arDados3['nomeseriedocumento']);
                                $usucri = $arDados3['siglausuariogerador'];
                                $conteudo = $arDados3['conteudo'];
                            } else {
                                //Mesmo processo mais de uma vez no bloco 
                                $tipodocu = utf8_encode($arDados3['nomeseriedocumento']);
                                if ($tipodocu == 'Conformidade sem Restrição') {
                                    $tipodocu1 = '<b>(SEM Restrição)</b>';
                                    $numdocsei = $arDados3['protocoloformatado'];
                                    $tipodocu = utf8_encode($arDados3['nomeseriedocumento']);
                                    $usucri = $arDados3['siglausuariogerador'];
                                    $conteudo = $arDados3['conteudo'];
                                    $numarDados3 = 1;
                                } else {
                                    $tipodocu1 = '<strong>Mais de um tipo encontrado(Com Restrição)</strong>';
                                    //Mesmo processo mais de uma vez no bloco com restrição
                                    $Arnumdocsei = Array();
                                    $Artipodocu = Array();
                                    $Arusucri = Array();
                                    $Arconteudo = Array();
                                    while ($arDados4 = mssql_fetch_array($Result4)) {
                                        $Arnumdocsei[] = $arDados4['protocoloformatado'];
                                        $Artipodocu[] = utf8_encode($arDados4['nomeseriedocumento']);
                                        $Arusucri[] = $arDados4['siglausuariogerador'];
                                        $Arconteudo[] = $arDados4['conteudo'];
                                    }
                                }
                            }

//FIM Confrontar com SEI e PEgar informaÃÂ§ÃÂµes                       

                            $diaupload = date("Y-m-d");

                            //INICIO Ver se ja existi o processo na tabela
                            $sqlsei = $objBanco->selectespe('sei', 'num_sei', $numsei);

                            $result = mysql_num_rows($sqlsei);


                            if ($result == false) {
                                $objBanco->sql = " INSERT INTO sei (`id_proto`, `num_sei`, `tipo_processo`, `uni_origem`, `local_origem`,`data_upload`)
                                  VALUES('" . $idprotocolo . "','" . $numsei . "','" . $tipoproce . "','" . $uniorigem . "','" . $idlocal . "','" . $diaupload . "')";
                                $rsProposto = $objBanco->query();
                            }

                            if ($numarDados3 == 1) {
                                $objBanco->sql = " INSERT INTO dados_sei (`id_proto`, `num_fav`, `nome_favo`, `valor`, `obs_plani`, `emi_dia`, `num_docsei`, `tipo_docu`, `usu_criador`, `doc_numsei`,`doc_ob`)
                                  VALUES('" . $idprotocolo . "','" . $nfavorecido . "','" . $nomefavorecido . "','" . $valor . "','" . $docobsv . "','" . $emi_dia . "','" . $numdocsei . "','" . $tipodocu . "','" . $usucri . "','" . $conteudo . "','" . $ndocumento . "')";
                                $rsProposto2 = $objBanco->query();
                            } else {

                                for ($c = 0; $c < count($Arnumdocsei); $c++) {
                                    $objBanco->sql = " INSERT INTO dados_sei (`id_proto`, `num_fav`, `nome_favo`, `valor`, `obs_plani`, `emi_dia`, `num_docsei`, `tipo_docu`, `usu_criador`, `doc_numsei`,`doc_ob`)
                                  VALUES('" . $idprotocolo . "','" . $nfavorecido . "','" . $nomefavorecido . "','" . $valor . "','" . $docobsv . "','" . $emi_dia . "','" . $Arnumdocsei[$c] . "','" . $Artipodocu[$c] . "','" . $Arusucri[$c] . "','" . $Arconteudo[$c] . "','" . $ndocumento . "')";
                                    $rsProposto2 = $objBanco->query();
                                }
                            }

                            $vouf = 'Verdadeiro';

                            // FIM Ver se ja existi o processo na tabela
                            //Inserindo na tabela dados_sei
                        } else {
                            
                            //Verificar se é despesa com pessoal
                        $Sql5 = "
SELECT DISTINCT protocolo.protocolo_formatado AS protocoloformatado, usu_ger.sigla AS siglausuariogerador, protocolo.dta_geracao AS geracao, 
serie.nome AS nomeseriedocumento, tpd.nome AS nometipoprocedimentodocumento, ppd.protocolo_formatado AS protocoloformatadoprocedime003, 
ppd.id_protocolo as id_protocolo_sei,rel_bloco_protocolo.anotacao,dc.conteudo
FROM protocolo
 left JOIN ( procedimento p INNER JOIN tipo_procedimento tpp ON p.id_tipo_procedimento = tpp.id_tipo_procedimento ) ON protocolo.id_protocolo = p.id_procedimento 
 LEFT JOIN ( documento INNER JOIN serie ON documento.id_serie = serie.id_serie 
 INNER JOIN documento_conteudo as dc ON dc.id_documento = documento.id_documento
 INNER JOIN ( procedimento pd INNER JOIN tipo_procedimento tpd ON pd.id_tipo_procedimento = tpd.id_tipo_procedimento 
 INNER JOIN protocolo ppd ON pd.id_procedimento = ppd.id_protocolo ) ON documento.id_procedimento = pd.id_procedimento ) ON protocolo.id_protocolo = documento.id_documento 
 INNER JOIN unidade uni_ger ON protocolo.id_unidade_geradora = uni_ger.id_unidade 
 INNER JOIN usuario usu_ger ON protocolo.id_usuario_gerador = usu_ger.id_usuario 
  INNER JOIN rel_bloco_protocolo ON rel_bloco_protocolo.id_protocolo = protocolo.id_protocolo
   WHERE rel_bloco_protocolo.id_bloco IN (64623) AND rel_bloco_protocolo.anotacao like '%$ndocumento%' ORDER BY protocolo.dta_geracao DESC;                                  
  
    ";
                        $Result5 = mssql_query($Sql5);
                        $contagem_despesas = mssql_num_rows($Result5);
                        
                        if($contagem_despesas>0){
                            $num_doc='29000200001'.$ndocumento;
                            
                              $objBanco->sql = " DELETE from n_sei WHERE num_doc = '$num_doc'";
                                $rsProposto = $objBanco->query();
                                
                             $vouf = 'OB DESPESAS COM PESSOAL';
                        }
                        else { 
                                              
                            
                            echo ' <b> NãO </b> encontrei : ' . $ndocumento;

                            if ($r[5] == '-9') {
                                //Inserindo na tabela nSEI
                                $objBanco->sql = " INSERT INTO n_sei (`num_doc`, `num_favo`, `nome_favo`, `valor`, `obs_plani`, `emi_di`,`id_local`)
                                  VALUES('" . $nducomentoori . "','" . $nfavorecido . "','" . $nomefavorecido . "','" . $valor . "','" . $docobsv . "','" . $emi_dia . "','" . $idlocal . "')";
                                $rsProposto = $objBanco->query();
                                // var_dump($rsProposto);
                                //Gravar dados na tabela N_sei
                                $vouf = 'Falso';
                            } else {
                                $vouf = 'OB CANCELADA';
                            }
                        } 
                        
                            }
                        //Salvando Ob cancelada
                        /*
                          if ($r[5] <> '-9') {
                          $ob_cancelada = $r[5];
                          $objBanco->sql = " INSERT INTO tb_ob_cancelada (`ob_cancelada`, `ob_atual`, `date_upload`)
                          VALUES('" . $ob_cancelada . "','" . $ndocumento . "','" . $diaupload . "')";
                          $rsProposto = $objBanco->query();

                          $objBanco->sql = " INSERT INTO tb_ob_cancelada_log (`ob_cancelada`)
                          VALUES('" . $ob_cancelada . "')";
                          $rsProposto = $objBanco->query();
                          } */

                        $msglog = 'Salvei a OB ' . $ndocumento . ', sendo ela ' . $vouf . ' e do tipo ' . @$tipodocu . ' / ' . @$tipodocu1;
                        unset($tipodocu);
                        unset($numdocsei);
                        unset($conteudo);
                        unset($tipodocu1);
                        logMsg($msglog, 'info');
                        echo '<br>';
                        //echo'------------------------';
                    }
                }


                for ($a = 0; $a < count($mesbanco); $a++) {
                    //Atualizando total_mes
                    $objBanco->sql = " UPDATE total_mes set `valort_mes` = $totalbanco[$a] WHERE `num_mes`= $mesbanco[$a] AND `ano` = $anobanco[$a]";
                    $rsProposto = $objBanco->query();

                    $msgbanco = 'Inseri no mes ' . $mesbanco[$a] . ' o valor de = ' . $totalbanco[$a];
                    logMsg($msgbanco, 'warning');
                }
                // Se houve mes novo/ Ano novo 
                if (count($nanobanco) > 0) {

                    for ($b = 0; $b < count($nmesbanco); $b++) {
                        //Inserindo na tabela total_mes
                        $objBanco->sql = " INSERT INTO total_mes (`num_mes`, `valort_mes`, `ano`)
                  VALUES('" . $nmesbanco[$b] . "','" . $ntotalbanco[$b] . "','" . $nanobanco[$b] . ")";
                        $rsProposto = $objBanco->query();
                    }
                }

                /*
                  $origem = $pasta.$ler;
                  $destino = '../public/arquivos/'.$ler;
                  copy($origem, $destino);
                  unlink($origem);
                 * *
                 */

//echo "<script>location.href='".RAIZ."admusu/indexadm'; </script>";
                require_once 'limpar.php';
                // require_once 'ob_cancelada.php';

                logMsg('Término da carga', 'fim');
                mssql_close();
                ?>

            </table>	


        </body>
    </html>
