

<!--TÍTULO E DIRETÓRIO DE NAVEGAÇÃO -->
<h1 class = "page-title">
    Área de Cadastramento de dados<small></small>
</h1>
<div class = "page-bar">
    <ul class = "page-breadcrumb">
        <li>
            <i class = "icon-home"></i>
            <span>Home</span>
            <i class = "fa fa-angle-right"></i>
        </li>
        <li>
            <a href = "<?php echo RAIZ . "inicio/inicio"; ?>">Página Início</a>
            <i class = "fa fa-angle-right"></i>
        </li>
        <li>
            <span>Diretório de navegação</span>
        </li>
    </ul>
    <div class = "page-toolbar">
        <div class = "btn-group pull-right">
            <a onclick = "window.history.go(-1)" class = "btn btn-fit-height grey-salt dropdown-toggle"><i class = "fa fa-reply"></i> Voltar </a>
        </div>
    </div>
</div>
<!--FIM TÍTULO E DIRETÓRIO DE NAVEGAÇÃO -->

<div class = "row">


    <div class = "col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">



        <!--ALERTAS -->
        <?php require HELPER . "mensagem.php";
        ?>
        <!-- FIM ALERTAS -->

        <form action="<?php echo CONTROLLER . 'inicio.php'; ?>" method="POST" enctype="multipart/form-data">

            <input type="hidden" name="arrDadosForm[method]" value="carga">
            <div class="portlet light ">
                <div class="portlet-body">

                    <div id="part1">
                        <h3 class="" id="texto1"></h3>
                        <input type="file" id="file" name="file[]"  accept=".xlsx" class="form-control">
                        <hr>
                    </div>

                    <div id="part2">
                        <h3 class="" id="texto2" name="arrDadosForm[tip_grafico]">Escolha o Tipo de Gráfico</h3>
                        <select class="form-control">
                            <option value=""></option>
                            <option value="1">Pizza</option>
                            <option value="2">Mapa - Brasil</option>
                        </select>
                        <hr>
                    </div>
                    <div id="selecionar_uf_mapa">
                        <h3 class="" id="texto3">Escolha a coluna da UF</h3>
                        <select class="form-control" id="select" name="arrDadosForm[coluna_uf]">

                        </select>
                    </div>
                    <div id="selecionar_dado_mapa">
                        <h3 class="" id="texto4">Selecione o Dado que deseja</h3>
                        <select class="form-control" id="select2" name="arrDadosForm[coluna_dado_uf]">
                        </select>
                    </div>
                    <div id="selecionar_titulo">
                        <h3 class="" id="texto5">Selecione o Titulo do Gráfico</h3>
                        <input type="text"  name="arrDadosForm[titulo]"   class="form-control">
                    </div>
                    <div id="selecionar_cor1">
                        <h3 class="" id="texto6">Selecione a cor inicial do seu gráfico</h3>
                        <input type="text"  name="arrDadosForm[cor1]"   class="jscolor form-control">
                    </div>
                    <div id="selecionar_cor2">
                        <h3 class="" id="texto7">Selecione a cor final do seu gráfico</h3>
                        <input type="text" class="jscolor form-control" name="arrDadosForm[cor2]">
                    </div>




                    <div class="form-actions right" style="margin-top:5%;">
                        <button type="button" class="btn default btn-circle">Cancelar</button>
                        <button type="submit" class="btn blue-madison btn-circle">
                            <i class="fa fa-check"></i> Salvar</button>
                    </div>

                </div>

            </div>

        </form>




    </div>
</div>




<script>
    $(document).ready(function() {

        $('#part3').hide();
        $('#part2').hide();
        $('#selecionar_uf_mapa').hide();
        $('#selecionar_dado_mapa').hide();
        $('#selecionar_titulo').hide();
        $('#selecionar_cor1').hide();
        $('#selecionar_cor2').hide();



        var typewriter = new Typewriter(texto1, {
            cursor: '|',
            blinkSpeed: 2
        });
        typewriter.typeString('Escolha seu arquivo XML')
                .pauseFor(2500)
                .start();


        $('input[type="file"]').change(function(event) {
            $('#part2').show();

            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);

            $.ajax({
                type: 'POST',
                dataType: 'text', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                url: '<?php echo CONTROLLER; ?>inicio.php',
                success: function(data) {
                    var response = $.parseJSON(data);

                    for (var i = 0; i < response.var_casa.length; ++i) {
                        var corrigido = response.linha[i];

                        $("#select").append('<option value=' + response.var_casa[i] + '>' + response.linha[i] + '</option>');
                        $("#select2").append('<option value=' + response.var_casa[i] + '>' + response.linha[i] + '</option>');
                    }
                }
            })
        });

        $('#part2').change(function() {
            $('#selecionar_uf_mapa').show();
        })

        $('#selecionar_uf_mapa').change(function() {
            $('#selecionar_dado_mapa').show();
        })

        $('#selecionar_dado_mapa').change(function() {
            $('#selecionar_titulo').show();
        })

        $('#selecionar_titulo').change(function() {
            $('#selecionar_cor1').show();
        })

        $('#selecionar_cor1').change(function() {
            $('#selecionar_cor2').show();
        })
    });




</script>
